//
//  SingleGuideController.swift
//  Guide
//
//  Created by Артём Шляхтин on 02/03/2017.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import UIKit

public struct Page {
    public var title = ""
    public var desc = ""
    public var imageName = ""
    
    public init() {
    }
}

public protocol SingleGuideDelegate: NSObjectProtocol {
    func guidePage(at index: Int) -> Page
    func guideNumberOfPages() -> Int
}

public class SingleGuideController: UIViewController {
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!

    public weak var delegate: SingleGuideDelegate?
    fileprivate var numberPages = 0
    
    // MARK: - Lifecycle

    public override func viewDidLoad() {
        super.viewDidLoad()
        configureNumberPages()
        configurePageControl()
        configureScrollView()
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation

    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    // MARK: - Action

    @IBAction func start(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Configuration
    
    func configureNumberPages() {
        if let count = delegate?.guideNumberOfPages() {
            numberPages = count
        }
    }
    
    func configurePageControl() {
        pageControl.numberOfPages = numberPages
        pageControl.currentPage = 0
    }
    
    func configureScrollView() {
        configureScrollViewContentSize()
        configureScrollViewObjects()
    }
    
    func configureScrollViewContentSize() {
        let width = self.scrollView.frame.size.width * CGFloat(numberPages)
        let height = self.scrollView.frame.size.height
        scrollView.contentSize = CGSize(width: width, height: height)
    }
    
    func configureScrollViewObjects() {
        var frame = CGRect.zero
        frame.size = self.scrollView.frame.size

        for index in 0..<numberPages {
            frame.origin.x = self.scrollView.frame.size.width * CGFloat(index)
            if let object = delegate?.guidePage(at: index) {
                let page = SingleGuideView(frame: frame)
                page.caption.text = object.title
                page.detail.text = object.desc
                page.imageView.image = UIImage(named: object.imageName)
                scrollView.addSubview(page)
            }
        }
    }
}

extension SingleGuideController: UIScrollViewDelegate {
    
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
}
