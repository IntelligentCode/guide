//
//  SingleGuideView.swift
//  Guide
//
//  Created by Артём Шляхтин on 02/03/2017.
//  Copyright © 2017 Артём Шляхтин. All rights reserved.
//

import UIKit

class SingleGuideView: UIView {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var caption: UILabel!
    @IBOutlet weak var detail: UILabel!
    
    // MARK: - initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        let view = loadViewFromNib()
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(identifier: "ru.roseurobank.Guide")
        let nib = UINib(nibName: "SingleGuideView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView
        return view
    }

}
